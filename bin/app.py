import web
import model
from web import form
urls = (
   '/', 'Index',
   '/del/(\d+)', 'Delete'
)

app = web.application(urls, globals())

render = web.template.render('templates/', base="layout")

class Index(object):
		
	iform = form.Form(     form.Textbox('name', form.notnull),
			       form.Textbox('task', form.notnull),
		   	       form.Button('submit'),
	)
	
		
	def GET(self):
		f = self.iform			
		todos = model.get_todos()
		return render.index(todos, f)
				

	def POST(self):
		f = self.iform()
		if not f.validates():
			todos = model.get_todos()
			return render.index(todos, f)

		model.new_todo(f.d.task, f.d.name)		

		raise web.seeother('/')		

class Delete:
	
	def POST(self, id):
		id = int(id)
		model.del_todo(id)
		raise web.seeother('/')


if __name__ == '__main__':
	app.run()
