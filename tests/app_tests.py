from nose.tools import *
from bin.app import app
from tests.tools import assert_response

def test_index():
	#we get 404 on the / url
	resp = app.request("/")
	assert_response(resp, status="200 OK")

